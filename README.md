[TOC]

### Quick Start

Clone this repository.

`cp .env.template .env`  
(Or copy the .env file from your oci-servers clone, if you are using that one, too.)

Edit `.env` with your information.

```
sh setup.sh`  
podman stop proxy && podman rm proxy
```

Edit `~/.local/share/containers/storage/volumes/swag-proxy/_data/dns-conf/` with you corrispoding DNS account information information.  

Copy desired sample file into a .conf file.  
Example:  
`cp ~/.local/share/containers/storage/volumes/swag-proxy/_data/nginx/proxy-confs/synapse.subdomain.conf.sample ~/.local/share/containers/storage/volumes/swag-proxy/_data/nginx/proxy-confs/synapse.subdomain.conf`

Edit your previously selected *.conf file to the corrisponding "pod" (such as matrix-pod for synapse or nextcloud-pod for nextcloud).

Check for your corrisponding server's special configs documenation below.  

`sh setup.sh`

Configure your NAT to move port 443 to this server's 8443, because this server is not built to run with root permissions.

Optionally, also NAT port 80 to 8080.

### Detailed Setup

##### Notes Before Beginning
This repository currently uses Linuxservers.io's Swag Proxy: https://github.com/linuxserver/docker-swag  
It is the best server I've found that obtains a letsencrypt HTTPS certificate automatically on startup.
The only problem that may occur is that linuxservers tend to make their own container images for all their servers.  
As a result, this container may have some configurations that conflict with the linuxservers' instructions. 
This is not common, but Nextcloud is an example of this. See the extra instructions for any more information you may want.

#### Table of Contents
* Initial Setup
* Nextcloud 
* Troubleshooting

# Initial Setup
Clone this repository:  
`git clone https://gitlab.com/CloudArtAdmins/oci-https-proxy.git` 

Go into the repository's folder and copy the `.env.template` file:    
`cp .env.template .env`

Fill in the variables with your corrisponding values.  
Example:  
```
SUBDOMAIN=nextcloud,matrix,otherSubdomain  
DOMAIN=my.domain
LETSENCRYPT_EMAIL=email@for.letsencrypt  
```

Run the setup script while in the same directory as the .env file and the proxy should function. 

Configure the container to use your DNS' credentials so it may automate the addition of a DNS TXT record for HTTPS.  
The files to add your DNS' API key can be found in `/home/user/.local/share/containers/storage/volumes/swag-proxy/_data/dns-conf`.  
NOTE: If you don't use any of these, cloudflare can be used without having purchased from them, but you may want to turn off the cloudflare proxy depending on which server you use.

Swag proxy uses config files found in the container's `nginx/proxy-confs/` folder. To add HTTPS to any of the servers, users will need to copy the sample file into the same directory without the ".sample" on the end.
Copy desired server's sample file into a .conf file. Example:  
`cp ~/.local/share/containers/storage/volumes/swag-proxy/_data/nginx/proxy-confs/synapse.subdomain.conf.sample ~/.local/share/containers/storage/volumes/swag-proxy/_data/nginx/proxy-confs/synapse.subdomain.conf`

Edit your previously selected *.conf file so it uses the corrisponding subdomain that you want and the corrispoding "pod" for your server in place of the default `$upstream_app`.  
The pod name can be found at the top of each `setup.sh` script, although the nominclature is expected to be "<server>-pod".

From here, just makes sure NAT is setup to move port 80 traffic to this server's port 8080, and 443 traffic to this server's 8443.  
These servers do not use root permissions, so they need to use ports above ~1023 on Linux. (Alternatively, you change your kernel restrictions, but if you are doing this you are probably experienced enough to already know how to this as well as its implications.)

Afterward, check for your corrisponding server's extra configurations below.  

Lastly,run the setup script while in the same directory as the .env file and the proxy should be functioning. 

You should be able to see if the containers are functioning by checking their status or by listing them and seeing if they are "up" for more than a couple of seconds.  
```
podman ps  
podman container ls -a
```

Assuming you have DNS and your firewall setup properly, the proxy's default page may look like this:  
![Image of Swag Proxy's default index](images/swag-proxy-default.png)

If you have your server running and the config file is correctly connected, you should see the proxied server's content.

##### Nextcloud
The configuration file `nextcloud.subdomain.conf`/`nextcloud.subdomain.conf.sample` shows default upstream information of "HTTPS" on port "443".  
This may work for the linuxservers.io nextcloud server, but not the default, latest nextcloud from the developers.  
Upstream variables under `location /` should appear as follows:  
```
set $upstream_app nextcloud-pod;  
set $upstream_port 80;
set $upstream_proto http;  
```

Carddav and caldav (Calendars and contacts) also are not configured by default.
If you would like to sync with nextcloud's calendar and contacts, add the following "locations" after `location / {}`  
```
location /.well-known/carddav {
    return 301 $scheme://$host/remote.php/dav;
}
location /.well-known/caldav {
    return 301 $scheme://$host/remote.php/dav;
}
```

##### Matrix
###### Federation Port
To federate with other matrix servers, you will want to add these lines with the corrisponding information to the synapse.subdomain.conf file that was made durring setup.

```
    location /.well-known/matrix/server {
        return 200 '{"m.server": "your-matrix.domain.here:443"}';
    }
```

### Troubleshooting
In general when running the script for the first time, my recommended method of troubleshooting is to view the individual container's logs.  
The proxy container itself: `podman logs proxy`  

#### Containers exit after closing my session/loging out.

This is assuming you want to run the script and keep the containers running.  
The alternative, probably more secure solution is to use a systemd unit, but assuming you don't want to do that....

As of systemd 230, you need to change your the default value of logind's variable `KillUserProcesses` to `no`.  
If you have the template of systemd-logind's configurations, you may just need to remove a comment `#`.

Edit the config file: `vim /etc/systemd/logind.conf`  
Add: `KillUserProcesses=no`   
Restart the service to apply configs: `sudo systemctl restart systemd-logind`  

Source: https://lwn.net/Articles/690166/
