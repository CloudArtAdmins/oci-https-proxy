#!/bin/sh 

podman network create proxy-net

podman pod create \
  --name=proxy-pod \
  -p 8443:443 \
  -p 8448:8448 \
  -p 8080:80 \
  --network proxy-net

podman run \
  --rm \
  -it \
  --name=certbot \
  --pod=proxy-pod \
  --cap-drop all \
  --volume letsencrypt-certs:/etc/letsencrypt:rw,z \
  docker.io/certbot/dns-cloudflare:latest \
  --agree-tos --dns-cloudflare certonly \
  --register-unsafely-without-email \
  --domain cloudart.moe \
  --domain dnd.cloudart.moe \
  --domain cloud.cloudart.moe \
  --domain matrix.cloudart.moe \
  --domain chat.cloudart.moe \
  --domain community.cloudart.moe \
  --dns-cloudflare-credentials /etc/letsencrypt/cloudflare.ini

podman run \
  --detach \
  --name=nginx \
  --pod=proxy-pod \
  --volume letsencrypt-certs:/etc/letsencrypt:ro,z \
  --volume site-confs:/etc/nginx/conf.d:ro,z \
  --volume nextcloud-app:/var/www/nextcloud:ro,z \
  docker.io/nginx:alpine
