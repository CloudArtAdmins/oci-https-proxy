#!/bin/sh

# See https://docs.linuxserver.io/general/swag#nextcloud-subdomain-reverse-proxy-example for configuration options
source ./.env &&

podman network create proxy-net

podman pod create \
  --name=proxy-pod \
  -p 8443:443 \
  -p 8448:8448 \
  -p 8080:80 \
  --network proxy-net

podman run \
  --detach \
  --name=proxy \
  --pod proxy-pod \
  -e TZ=US/Arizona \
  -e URL="$DOMAIN" \
  -e SUBDOMAINS="$SUBDOMAINS" \
  -e VALIDATION=dns \
  -e DNSPLUGIN=cloudflare \
  -e EMAIL="$LETSENCRYPT_EMAIL" \
  -v swag-proxy:/config \
  --restart unless-stopped \
  --pull always \
  ghcr.io/linuxserver/swag

